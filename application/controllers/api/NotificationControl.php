<?php  
	
	define("FIREBASE_SERVER_KEY", "AAAAVLn8YMg:APA91bHD7VjzULhF-6ojPE2kgErySLonvHSPozbkR3UvaaKOhJaOBZsquRHgWD2V7xChd-x7HG_omsDoiA4FMMoguERjl7SLtiuDjA4MYVQUHW3vIpLSnHJE62uIEVwWKLyssBPPSU6G");
  	
  	class NotificationControl extends Core{

    	protected static $_user_register_id;

    	public function __construct(){ 
      		parent::__construct();
    	}

    	private static function send($mobile, $title, $message){
    		$search_user_firebase_auth = RapidDataModel::read("user_firebase_auth", [
	    		"where" => [
	    			"mobile" => Core::__Body_Request()["mobile"]
	    		]
	    	])["rows"];
	    	if (count($search_user_firebase_auth) > 0) {
	    		$icon = Core::$_Controller->config->item("base_url") . "favicon.png";
				$payload = array( 
					'to' => $search_user_firebase_auth[0]["device_token"], 
					'priority' => 'high', 
					"mutable_content" => true,
					 "notification" => array( 
					 	"icon" => $icon, 
					 	"title" => $title, 
					 	"body" => $message,			
					 	"click_action" => "com.mbv.pokket_INBOX3ACTIVITY"
					),
				);
				$headers = array( 'Authorization:key='. FIREBASE_SERVER_KEY, 'Content-Type: application/json');	
				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
				$result = json_decode(curl_exec($ch ));
				curl_close( $ch ); 
				// print_r($result)
				if ($result->success == 1) {
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
    	}

    	// Send Through internal
    	public static function internal_send($mobile, $title, $message){
    		return self::send($mobile, $title, $message);
    	}

    	// Send through Outside API 
    	public static function _send(){
	    	if (Core::__Required_Params(["mobile", "title", "message"])) {
	    		$send = self::send(Core::__Body_Request()["mobile"], Core::__Body_Request()["title"], Core::__Body_Request()["message"]);
	    		if ($send) {
	    			 Core::__Send_Output([
						"status" => true,
						"message" => "Notification Pushed",
						"data" => [
							"title" => Core::__Body_Request()["title"],
							"message" => Core::__Body_Request()["message"],
				  			"click_action" => "com.mbv.pokket_INBOX3ACTIVITY"
						]
					]);
	    		}else{
	    			Core::__Send_Output([
						"status" => false,
						"message" => "Failed pushing notification",
					]);
	    		}
    		}
    	}

	    public static function _send_global(){
	    	if (Core::__Required_Params(["title", "message"])) {
	    		$data = json_decode(Core::__Body_Request()["data"] ?? null) ?? null;
	    		$icon = Core::$_Controller->config->item("base_url") . "favicon.png";
		      	$payload = array(
		      		'to' => '/topics/global', 
		      		'priority'=>'high', 
		      		"mutable_content" => true,
		          	"notification" => array( "icon" => $icon, "title" => Core::__Body_Request()["title"], "body" =>  Core::__Body_Request()["message"],
		          	"click_action" => "com.mbv.pokket_INBOX3ACTIVITY"
		          ),
		          	'data'=> $data
		        );
			    $headers = array( 'Authorization:key='. FIREBASE_SERVER_KEY, 'Content-Type: application/json');
			    $ch = curl_init();
			    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
			    curl_setopt( $ch,CURLOPT_POST, true );
			    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $payload ) );
			    $result = curl_exec($ch );
			    curl_close( $ch );
		      	if ($result) {
				    Core::__Send_Output([
						"status" => true,
						"message" => "Notification Pushed",
						"data" => [
							"icon" => $icon,
							"result" => json_decode($result)->message_id,
							"title" => Core::__Body_Request()["title"],
							"message" => Core::__Body_Request()["message"],
							"click_action" => "com.mbv.pokket_INBOX3ACTIVITY"
						]
					]);
				}else{
				    Core::__Send_Output([
						"status" => false,
						"message" => "Failed pushing notification",
					]);
				}
		  	}
    	}
  	}
?>
