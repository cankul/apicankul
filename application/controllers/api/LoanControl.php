<?php  
	class LoanControl extends Core{

        protected static $_loan_minimum_amount;
        protected static $_loan_maximum_amount;
		protected static $_loan_default_amount;

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		public
			static
				function 
					_Product(){
                        $select = "

                       FLOOR( ((maximum_principal*(maximum_interest_rate/100))*default_loan_duration) + maximum_principal ) as total_payment,

                            id, user_id, name, loan_disbursed_by_id, FLOOR(minimum_principal) as minimum_principal, FLOOR(default_principal) as default_principal, FLOOR(maximum_principal) as maximum_principal , interest_method, interest_rate, interest_period, minimum_interest_rate,  default_interest_rate, maximum_interest_rate, override_interest, FLOOR(override_interest_amount) as override_interest_amount, default_loan_duration, default_loan_duration_type, repayment_cycle, decimal_places, repayment_order, loan_fees_schedule, branch_access, grace_on_interest_charged, advanced_enabled, enable_late_repayment_penalty, enable_after_maturity_date_penalty, after_maturity_date_penalty_type, late_repayment_penalty_type, late_repayment_penalty_calculate, after_maturity_date_penalty_calculate, late_repayment_penalty_amount, after_maturity_date_penalty_amount, late_repayment_penalty_grace_period, after_maturity_date_penalty_grace_period, late_repayment_penalty_recurring, after_maturity_date_penalty_recurring, accounting_rule, after_maturity_date_penalties, after_maturity_date_penalty_system_type";
                        if (empty(Core::__Body_Request()["id"])) {
                            $condition = array(
                                "select" => $select,
                                "order"=> ['name','ASC']
                            );
                            $loan_product = RapidDataModel::read('loan_products', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product
                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["id" => Core::__Body_Request()["id"]],
                                "order"=> ['name','ASC']
                             );
                            $loan_product = RapidDataModel::read('loan_products', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product[0]
                            ]);
                        }

                    }

        private 
            static
                function
                    __Validate_Amount($amount){
                        // If amount user entered less than minimum amount of product
                        if ($amount < self::$_loan_minimum_amount) {
                            // Return false
                            return false;
                        }else{
                            // If amount user entered more than maximum amount of product
                            if ($amount > self::$_loan_minimum_amount) {
                                // return false
                                return false;
                            }else{
                                // Amount is acceptable
                                return true;
                            }
                        }
                    }


        private static function __Create_Loan_Data($user_data, $prod_id, $amount, $disbursement_id, $account_number, $account_name, $bank_id, $document_id ){
            
            $product_data = RapidDataModel::read("loan_products", [
                "where" => ["id" => $prod_id]
            ])["rows"][0];

            $create_data = [
              "user_id" => rand(0,9999),
              "borrower_id" => $user_data["id"],
              "loan_product_id" => $prod_id,
              "reference" => "",
              "release_date" => date("Y-m-d H:i:s"),
              "maturity_date" => date("Y-m-d H:i:s"),
              "month" => date("m"),
              "year" => date("Y"),
              "interest_start_date" => "",
              "first_payment_date" => date("Y-m-d H:i:s"),
              "loan_disbursed_by_id" => "",
              "principal" => $product_data["maximum_principal"],
              "interest_method" => $product_data["interest_method"],
              "interest_rate" => $product_data["default_interest_rate"],
              "interest_period" => $product_data["interest_period"],
              "override_interest" => $product_data["override_interest"],
              "override_interest_amount" => $product_data["override_interest_amount"],
              "loan_duration" => $product_data["default_loan_duration"],
              "loan_duration_type" => $product_data["default_loan_duration_type"] ,
              "repayment_cycle"  => $product_data["repayment_cycle"],
              "decimal_places"  => $product_data["decimal_places"],
              "repayment_order" => $product_data["repayment_order"],
              "loan_fees_schedule"  => $product_data["loan_fees_schedule"] ?? "distribute_fees_evenly",
              "grace_on_interest_charged" => $product_data["grace_on_interest_charged"],
              "loan_status_id" => null,
              "files" => "a:0:{}",
              "description" => "",
              "loan_status" => "open",
              "balance" => NULL,
              "override" => 0,
              "created_at" => date("Y-m-d H:i:s"),
              "updated_at" => date("Y-m-d H:i:s"),
              "deleted_at" => null,
              "status" => "approved",
              "applied_amount" => $amount,
              "approved_amount" => $amount,
              "approved_notes" => NULL,
              "disbursed_notes" => NULL,
              "withdrawn_notes" => NULL,
              "closed_notes" => NULL,
              "rescheduled_notes" => NULL,
              "declined_notes" => NULL,
              "written_off_notes" => NULL,
              "approved_date" => date("Y-m-d H:i:s"),
              "disbursed_date" => NULL,
              "withdrawn_date" => NULL,
              "closed_date" => NULL,
              "rescheduled_date" => NULL,
              "declined_date" => NULL,
              "written_off_date" => NULL,
              "approved_by_id" => NULL,
              "disbursed_by_id" => NULL,
              "withdrawn_by_id" => NULL,
              "declined_by_id" => NULL,
              "written_off_by_id" => NULL,
              "rescheduled_by_id" => NULL,
              "closed_by_id" => NULL,
              "branch_id" => 1,
              "processing_fee" => "",
              "product_check_out_id" => "",
              "disbursement_id" => $disbursement_id,
              "account_number" => $account_number,
              "account_name" => $account_name,
              "bank_id" => $bank_id,
              "document_id" => $document_id

            ];

            $save =  RapidDataModel::insert("loans", $create_data,id);
            
            if ($save) {
              
                return $save;
            }else{
                return false;
            }

        }

        private
            static 
                function
                    __Create_Aplly_Data(){


                        $create_data = RapidDataModel::create("loan_applications", [
                            [
                                "borrower_id" => Core::__Body_Request()["borrower_id"],
                                "loan_product_id" => Core::__Body_Request()["loan_product_id"],
                                "amount" => Core::__Body_Request()["amount"],
                                "status" => Core::__Body_Request()["status"] ?? "pending",
                                "notes" => Core::__Body_Request()["notes"] ?? "",
                                "created_at" =>  date("Y:m:d h:i:s"),
                                "updated_at" => date("Y:m:d h:i:s"),
                                "branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
                                "disbursement_id" => Core::__Body_Request()["disbursement_id"] ?? "",
                                "account_number" => Core::__Body_Request()["account_number"] ?? "",
                                "account_name" => Core::__Body_Request()["account_name"] ?? "",
                                "bank_id" => Core::__Body_Request()["bank_id"] ?? "",
                                "document_id" => Core::__Body_Request()["document_id"] ?? "",


                            ]
                        ]);
                        if (!empty($create_data)) {
                            Core::__Send_Output([
                                "status" => true,
                                "data" => [
                                    "id" => $create_data
                                ],
                                "message" => "Loan succesfully applied, Now waiting for approval"
                            ]);
                        }else{
                            Core::__Send_Output([
                                "status" => false,
                                "message" => API_FAILED_INSERT_DATABASE,
                                "message_error" => "Failed inserting data, please try again."
                            ]);
                        }
                    }

        private
            static 
                function
                    __Create_Aplly_Data_OCR($args){
                        $user_data = RapidDataModel::read("borrowers", [
                            "where" => [
                                "mobile" => Core::__Body_Request()["mobile"]
                            ]
                        ])["rows"];

                        


                        $loans = RapidDataModel::read("loans", [
                            "where" => [
                                "borrower_id" => $user_data[0]["id"]
                            ],
                             "order" => ["loans.updated_at", "DESC"]
                        ])["rows"];
                        
                         $param_cicilan = RapidDataModel::read("param_cicilan")["rows"];

                        
                        $interest_rate_per_day = ($loans[0]["approved_amount"] * $loans[0]["interest_rate"]) + $loans[0]["approved_amount"]  ;
                        $interest_rate_total   = $interest_rate_per_day * $loans[0]["loan_duration"];
                      
                      // cek total applied
                        $select1 = "*";
                        $condition1 = array(
                                "select" => $select1,
                                "where" => [ "loan_id" => $loans[0]["id"]],

                                "order" => ["loan_transactions.updated_at", "DESC"]
                            );
                        $applied = RapidDataModel::read('loan_transactions', $condition1)["rows"];

                        // cek repayment_order
                        $repayments = RapidDataModel::read("loan_transactions", [
                            "where" => [
                                "loan_id" => $loans[0]["id"],
                                "transaction_type"=>"repayment",
                            ],
                             "order" => ["loan_transactions.updated_at", "DESC"]
                        ])["rows"];
                       
                        $total = 0 ;
                        for ($i=0; $i < count($repayments); $i++) { 
                        
                            $credit += $repayments[$i]['credit'];
                      
                        }

                        $debet = 0 ;
                        for ($i=0; $i < count($applied); $i++) { 
                        
                            $debet += $applied[$i]['debit'];
                      
                        }


                        $presentation =  ($credit / $debet)* 100 ;
                        if (is_nan($presentation)) { $pres = 0;}else{ $pres = $presentation; }
                        // print_r($applied);
                        // print_r($repayments);
                        // print_r(Core::__Body_Request()["mobile"]);
                        // print_r($loans[0]["id"]);
                    //     // print_r($loans);
                    // echo "credit :::";    print_r($credit);
                    // echo "debet :::";      print_r($debet);
                    //     print_r($presentation);
                    //     die();

                        if (count($loans) < 1) {
                             if ($user_data[0]["blacklisted"] == 1) {
                                  Core::__Send_Output([
                                      "status" => false,
                                      "message" => "Your account has been blacklisted, Please contact our service if you feel this not right",
                                  ]);
                              }else{
                                  $create_data = RapidDataModel::create("loan_applications", [
                                      [
                                          "borrower_id" => $user_data[0]["id"],
                                          "loan_product_id" => Core::__Body_Request()["loan_product_id"],
                                          "amount" => $args["amount"],
                                          "status" => "approved",
                                          "notes" => Core::__Body_Request()["notes"] ?? "",
                                          "created_at" =>  date("Y:m:d h:i:s"),
                                          "updated_at" => date("Y:m:d h:i:s"),
                                          "branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
                                          "disbursement_id" => Core::__Body_Request()["disbursement_id"] ?? "",
                                          "account_number" => Core::__Body_Request()["account_number"] ?? "",
                                          "account_name" => Core::__Body_Request()["account_name"] ?? "",
                                          "bank_id" => Core::__Body_Request()["bank_id"] ?? "",
                                          "document_id" => Core::__Body_Request()["document_id"] ?? "",


                                      ]
                                  ]);
                                  if (!empty($create_data)) {
                                    $Create_Data_Loans = self::__Create_Loan_Data($user_data[0], Core::__Body_Request()["loan_product_id"], $args["amount"] , Core::__Body_Request()["disbursement_id"],Core::__Body_Request()["account_number"], Core::__Body_Request()["account_name"], Core::__Body_Request()["bank_id"],Core::__Body_Request()["document_id"]  );
                                    // die();
                                      // Create Data Loans
                                      if ($Create_Data_Loans) {
                                          // Send Notification to users app
                                          // NotificationControl::send(
                                          //     Core::__Body_Request()["mobile"], 
                                          //     "Fund Application Status",
                                          //     "Thank you for your fund application in Cankul. To see the fund approval, please check your HISTORY page."
                                          // );
                                            
                                                 // Send SMS to User Borrower AND VA in table
                                                //  $code = substr(str_shuffle("1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm@$!"), 0, 6);
                                                $ktp = Core::__Body_Request()["mobile"];
                                                $ktp_code = substr($ktp,6);
                                                $datenow = date('His');
                                                $code = $ktp_code .''. $datenow;
                                                $code_VA = $code;
                                                 $create_code_disbrusment_loan = RapidDataModel::create("code_disbrusment_loan", [[
                                                     "loan_id"=>$Create_Data_Loans,
                                                     "mobile"=>Core::__Body_Request()["mobile"],
                                                     "code"=>$code_VA,
                                                     "date_updated"=> date("Y:m:d h:i:s")
                                                     ]]);
                                                 $send_otp = OtpControl::__Send_Short_Message(Core::__Body_Request()["mobile"], 
                                                 "Selamat pengajuan pinjaman anda telah disetujui,
                                                 Silahkan lakukan penarikan Dana Pinjaman Anda melalui
                                                 ( Bank Transfer, Alfamart, BNI Virtual Account ) dengan kode " .
                                                  $code . " trima kasih.");
                                              

                                          Core::__Send_Output([
                                              "status" => true,
                                              "accepted" => true,
                                              "message_eng-us" => "Thank you for your fund application in Cankul. To see the fund approval, please check your HISTORY page.",
                                              "message" => "Terima kasih atas aplikasi pendanaan di Cankul. Untuk melihat persetujuan pendanaan, silakan cek laman HISTORY anda.",
                                              "data" => [
                                                  "id" => $create_data
                                              ]
                                          ]);
                                      }else{
                                          Core::__Send_Output([
                                              "status" => false,
                                              "message" => API_FAILED_INSERT_DATABASE,
                                              "message_error" => "Failed inserting data, please try again."
                                          ]);
                                      }
                                  }else{
                                      Core::__Send_Output([
                                          "status" => false,
                                          "message" => API_FAILED_INSERT_DATABASE,
                                          "message_error" => "Failed inserting data, please try again."
                                      ]);
                                  }
                              }
                          
                        }else{
                           if ($presentation > $param_cicilan[0]["nilai"]  ) {

                                 if ($user_data[0]["blacklisted"] == 1) {
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => "Your account has been blacklisted, Please contact our service if you feel this not right",
                                        ]);
                                    }else{
                                        $create_data = RapidDataModel::create("loan_applications", [
                                            [
                                                "borrower_id" => $user_data[0]["id"],
                                                "loan_product_id" => Core::__Body_Request()["loan_product_id"],
                                                "amount" => $args["amount"],
                                                "status" => "approved",
                                                "notes" => Core::__Body_Request()["notes"] ?? "",
                                                "created_at" =>  date("Y:m:d h:i:s"),
                                                "updated_at" => date("Y:m:d h:i:s"),
                                                "branch_id" => Core::__Body_Request()["branch_id"] ?? 1,
                                                "disbursement_id" => Core::__Body_Request()["disbursement_id"] ?? "",
                                                "account_number" => Core::__Body_Request()["account_number"] ?? "",
                                                "account_name" => Core::__Body_Request()["account_name"] ?? "",
                                                "bank_id" => Core::__Body_Request()["bank_id"] ?? "",
                                                "document_id" => Core::__Body_Request()["document_id"] ?? "",

                                            ]
                                        ]);
                                        if (!empty($create_data)) {
                                           $Create_Data_Loans = self::__Create_Loan_Data($user_data[0], Core::__Body_Request()["loan_product_id"], $args["amount"] , Core::__Body_Request()["disbursement_id"],Core::__Body_Request()["account_number"], Core::__Body_Request()["account_name"], Core::__Body_Request()["bank_id"],Core::__Body_Request()["document_id"]  );
                                            // die();

                                            // Create Data Loans
                                            if ($Create_Data_Loans) {
                                                // Send Notification to users app
                                                // NotificationControl::send(
                                                //     Core::__Body_Request()["mobile"], 
                                                //     "Fund Application Status",
                                                //     "Thank you for your fund application in Cankul. To see the fund approval, please check your HISTORY page."
                                                // );

                                                 // Send SMS to User Borrower AND VA in table
                                                //  $code = substr(str_shuffle("1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm@$!"), 0, 6);

                                                $ktp = Core::__Body_Request()["mobile"];
                                                $ktp_code = substr($ktp,6);
                                                $datenow = date('His');
                                                $code = $ktp_code .''. $datenow;
                                                $code_VA = $code;
                                                 $create_code_disbrusment_loan = RapidDataModel::create("code_disbrusment_loan", [[
                                                     "loan_id"=>$Create_Data_Loans,
                                                     "mobile"=>Core::__Body_Request()["mobile"],
                                                     "code"=>$code_VA,
                                                     "date_updated"=> date("Y:m:d h:i:s")
                                                     ]]);
                                                 $send_otp = OtpControl::__Send_Short_Message(Core::__Body_Request()["mobile"], 
                                                 "Selamat pengajuan pinjaman anda telah disetujui,
                                                 Silahkan lakukan penarikan Dana Pinjaman Anda melalui
                                                 ( Bank Transfer, Alfamart, BNI Virtual Account ) dengan kode " .
                                                  $code . " trima kasih.") ;
          
                                                 
                                                Core::__Send_Output([
                                                    "status" => true,
                                                    "accepted" => true,
                                                    "message_eng-us" => "Thank you for your fund application in Cankul. To see the fund approval, please check your HISTORY page.",
                                                    "message" => "Terima kasih atas aplikasi pendanaan di Cankul. Untuk melihat persetujuan pendanaan, silakan cek laman HISTORY anda.",
                                                    "data" => [
                                                        "id" => $create_data
                                                    ]
                                                ]);
                                            }else{
                                                Core::__Send_Output([
                                                    "status" => false,
                                                    "message" => API_FAILED_INSERT_DATABASE,
                                                    "message_error" => "Failed inserting data, please try again."
                                                ]);
                                            }
                                        }else{
                                            Core::__Send_Output([
                                                "status" => false,
                                                "message" => API_FAILED_INSERT_DATABASE,
                                                "message_error" => "Failed inserting data, please try again."
                                            ]);
                                        }
                                    }
                            }else{
                               Core::__Send_Output([
                                                "status" => false,
                                                "accepted" => false,
                                                "message" => "Cicilan Anda Sebelumnya kurang dari ". floor($param_cicilan[0]["nilai"])."% dan cicilan anda =". $pres."%" ,
                                            ]);
                            }
                        }
                       
                    }
     
        public
            static
                function
                    _Apply_Ocr(){
                        if (Core::__Required_Params(["loan_product_id", "mobile"])) {
                           if (!RapidDataModel::is_exist('loan_products', ["id" => Core::__Body_Request()["loan_product_id"]])) {
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => API_FAILED_404_DATA,
                                ]);
                            }else{
                                // Product Score Ranges
                                $get_user_score = RapidDataModel::read("advance_ai_ocr", [
                                    "where" => [
                                        "mobile" => Core::__Body_Request()["mobile"],
                                        "endpoint" => "OCR_CREDIT_SCORE"
                                    ]
                                ])["rows"];
                                $user_score = json_decode($get_user_score[0]["raw_json"])->score ?? 0;
                                $user_selected_product = Core::__Body_Request()["loan_product_id"];
                                $product_range = RapidDataModel::read("loan_product_range_mapping", [])["rows"];
                                $accepted_products = [];
                                foreach ($product_range as $key => $value) {
                                    if (in_array($user_score, range($value["min_range"], $value["max_range"]))) {
                                        $accepted_products [] = $value["loan_product_id"];
                                    }else{
                                        $other_min_range = $value["min_range"];
                                        $other_max_range = $value["max_range"];
                                        if ($user_score >= $other_min_range) {
                                            $accepted_products [] = $value["loan_product_id"];
                                        }
                                    }
                                }
                                if (in_array($user_selected_product, $accepted_products)) {
                                    // Accepted Application 
                                    $product_detail = RapidDataModel::read("loan_products", [
                                        "select" => "name, maximum_principal as amount, interest_method",
                                        "where" => [
                                            "id" => Core::__Body_Request()["loan_product_id"]
                                        ]
                                    ])["rows"][0];
                                    // Create Apply Data
                                    self::__Create_Aplly_Data_OCR([
                                        "amount" => $product_detail["amount"]
                                    ]);
                                }else{
                                    if (!empty($accepted_products)) {
                                        $product_offers = [];
                                        foreach ($accepted_products as $value) {
                                            $product_detail = RapidDataModel::read("loan_products", [
                                                "select" => "id, name, maximum_principal as amount, interest_method, interest_period, default_loan_duration_type",
                                                "where" => [
                                                    "id" =>  $value
                                                ]
                                            ])["rows"];
                                            $product_offers [] = $product_detail[0];
                                        }
                                        Core::__Send_Output([
                                            "status" => true,
                                            "accepted" => false,
                                            "message_eng-us" => "Sorry, Cankul cannot proceed your fund application. Please try again later",
                                            "message" => "Pendanaan tidak dapat disetujui, berdasarkan credit scoring Anda. Berikut adalah penawaran lain dari kami",
                                            "data" => $product_offers
                                        ]);
                                    }else{
                                        Core::__Send_Output([
                                            "status" => false,
                                            "accepted" => false,
                                            "message" => "Your application does not meet our requirements",
                                        ]);
                                    }
                                }
                            }   
                        }
                    }

        public 
            static
                function
                    _Apply(){
                        if (Core::__Required_Params(["loan_product_id", "amount", "borrower_id"])) {
                            if (!RapidDataModel::is_exist('loan_products', ["id" => Core::__Body_Request()["loan_product_id"]])) {
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => API_FAILED_404_DATA,
                                ]);
                            }else{
                                $select = "*";
                                $condition = array(
                                        "select" => $select,
                                        "where" => ["id" => Core::__Body_Request()["loan_product_id"]]
                                    );
                                $loan_product = RapidDataModel::read('loan_products', $condition)["rows"][0];
                                self::$_loan_minimum_amount = intval($loan_product["minimum_principal"]);
                                self::$_loan_default_amount = intval($loan_product["default_principal"]);
                                self::$_loan_maximum_amount = intval($loan_product["maximum_principal"]);
                                // print_r(self::$_loan_minimum_amount);
                                if (self::__Validate_Amount(Core::__Body_Request()["amount"])) {
                                    // Create application data
                                    self::__Create_Aplly_Data();
                                }else{
                                    Core::__Send_Output([
                                         "status" => false,
                                            "message" => API_FAILED_PARAMETER_VALIDATION,
                                            "message_error" => "Amount you entered does not complied to our product's minimum or maximum amount"
                                    ])  ;
                                }
                            }
                        }
                    }
        public
            static
                function 
                    _Myapplication(){
                        // print_r("test");
                        // die()

                        $select = "loan_applications.id as id, loan_products.name as loan_product_name, FLOOR(loan_applications.amount) as amount, loan_applications.status as status, loan_applications.notes as notes , loan_applications.created_at as date , loan_applications.updated_at";
                        

                        if (empty(Core::__Body_Request()["borrower_id"])) {
                            
                            $condition = array(
                                "select" => $select, 
                                "order" => ["loan_applications.updated_at", "DESC"]
                            );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"],"status !=" => "approved"],
                                "join" => array(
                                        "borrowers"     => "loan_applications.borrower_id = borrowers.id",
                                        "loan_products" => "loan_applications.loan_product_id = loan_products.id"
                                ),
                                "order" => ["loan_applications.updated_at", "DESC"]
                             );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                        } 
                    }
         public
            static
                function 
                    _Riwayatpinjaman(){
             

                        $select = "loan_products.name,
                                    loans.loan_duration,
                                    loan_products.default_loan_duration_type,
                                    FLOOR(loans.principal) as principal,
                                   loans.first_payment_date,
                                    loans.status,
                                    loans.loan_status,
                                      loans.updated_at,

                        ";

                        if (empty(Core::__Body_Request()["borrower_id"])) {
                             Core::__Send_Output([
                                "status" => true,
                                // "data" => $loan_product,
                                "keterangan" => "borrower_id kosong"

                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array(
                                        "borrowers"     => "loans.borrower_id = borrowers.id",
                                        "loan_products" => "loans.loan_product_id = loan_products.id"
                                ),
                                "order" => ["loans.updated_at", "DESC"]
                             );
                            $loan_product = RapidDataModel::read('loans', $condition)["rows"];
                             if (empty($loan_product)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                                "keterangan" => "data kosong"

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                            }
                        } 
                }
        public
			static
				function 
					_Repayment(){
                     
                        $select = " CURRENT_DATE + 7  , loan_transactions.date as date,
                                    loan_repayment_methods.name as name_method,
                                   FLOOR(loan_transactions.credit) as credit,
                                   loan_repayments.due_date as due_date";  
                      
                       

                            $condition = array(
                                "select" => $select,
                                "where" => ["borrowers.id" => Core::__Body_Request()["borrower_id"] ],
                                "join" => array(
                                        "loans" => "loans.borrower_id = borrowers.id",
                                        "loan_transactions" => "loans.id = loan_transactions.loan_id",
                                        "branches" => "branches.id = loan_transactions.branch_id",
                                        "users" => "users.id = loan_transactions.user_id",
                                        "loan_repayment_methods" => "loan_repayment_methods.id = loan_transactions.repayment_method_id",
                                        "loan_repayments" => "loan_repayments.borrower_id = borrowers.id",
                                        
                                ),
                                "order" => ["loan_transactions.updated_at", "DESC"]
                             );
                            $loan_product = RapidDataModel::read('borrowers', $condition)["rows"];
                            
                            if (empty($loan_product)) {
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                                "message" => "Repayment not found"

                            ]);
                            }else{
                                Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                            }

					  

                    }
	}
  