<?php  
    require_once APPPATH .'libraries/twilio/vendor/autoload.php'; 
    use Twilio\Rest\Client;
	class OtpControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

        private 
            static
                function
                    internationalize_phone_number($phone){
                        $indonesia_id = 62;
                        $phone_first_digit = substr($phone, 0,1);
                        if ($phone_first_digit == 0) {
                            $phone_number = substr($phone, 1);
                            return $indonesia_id . $phone_number;
                        }else{
                            return $phone;
                        }
                    }

        // public 
        //     static
        //         function
        //             __Send_Short_Message($phone, $message){
        //                 $sid = "ACb168745482c2d32b0a114589f5cb4ac3";
        //                 $token = "4c7231aedbf8bafcef4e6da57473c19d";
        //                 try {
        //                     $twilio = new Client($sid, $token);
        //                     $send_SMS = $twilio->messages->create("+". self::internationalize_phone_number($phone) , array(
        //                         "From" =>   "+18023919294",
        //                         "Body" =>  $message
        //                     ));  
        //                    if ($send_SMS) {
        //                         return true;
        //                    }else{
        //                         return false;
        //                    }
        //                 } catch (Exception $e) {
        //                     return false;
        //                 }
        //             }

            public static function __Send_Short_Message($phone, $message){

               $curl = curl_init();

                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://api.cankul.com",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"keys\"\r\n\r\np3463y\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"phone_number\"\r\n\r\n".$phone."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"messages_text\"\r\n\r\n".$message."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
                  CURLOPT_HTTPHEADER => array(
                    "Postman-Token: e5ed312d-1b10-4291-905c-ef78c7751154",
                    "cache-control: no-cache",
                    "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
                  ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                  return $err;
                } else {
                  return json_decode($response);
                }
            }

		public
			static
				function 
					_Send(){
                        if (Core::__Required_params(array("mobile"))) {
                            $numbers = "0987654321";
                            $charUppercase = "QWERTYUIOPASDFGHJKLZXCVBNM";
                            $otp_expired = date("Y-m-d H:i:s", strtotime("+ 5 minutes"));
                            $otp = substr(str_shuffle(str_shuffle($numbers.$charUppercase)), 0, 5);
                            $message = "Kode OTP Anda adalah " . $otp. ", Berlaku hingga ". $otp_expired;

                            $is_exist = RapidDataModel::read("sms_otp", [
                                "where" => [
                                    "phone" => Core::__Body_Request()["mobile"]
                                ]
                            ])["rows"];

                            // If data sms is not exist in database
                            if (empty($is_exist)) {
                                // Insert data to database
                                $otp_in_database = RapidDataModel::insert('sms_otp', [
                                    "phone" => Core::__Body_Request()["mobile"],
                                    "code" => $otp,
                                    "date_expired" => $otp_expired
                                ]);
                                // If its inserted
                                if ($otp_in_database) {
                                    // Send SMS
                                // print_r(self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message)->status );
                                // die();

                                    if (self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message)->status == 202) {
                                        Core::__Send_Output([
                                            "status" => true,
                                            "message" => $message,
                                            "data" => [
                                                "otp" => $otp,
                                                "code" => $otp_expired
                                            ]
                                        ]);
                                    }else{
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => "Failed sending message, Insufficient Zenziva Credit",
                                        ]);
                                    }
                                }
                            }else{
                                // if data already exist
                                // Means that user already send OTP to their phone
                                $sms = RapidDataModel::read('sms_otp', [
                                    "where" => [
                                        "phone" => Core::__Body_Request()["mobile"]
                                    ]
                                ]);
                                // Check wether is not expired anymore
                                // If its expired
                                if (!empty($sms["rows"])) {
                                    if (strtotime(date("Y-m-d H:i:s")) > strtotime($sms["rows"][0]["date_expired"])) {
                                        // Update sms data
                                        // renew otp code and date_expired
                                        $renew = RapidDataModel::update('sms_otp', [
                                            "key" => ["phone" => Core::__Body_Request()["mobile"]],
                                            "data" => [
                                                "code" => $otp,
                                                "date_expired" => date("Y-m-d H:i:s", strtotime("+ 5 minutes"))
                                            ]
                                        ]);
                                        if ($renew) {
                                            // Send otp
                                            if (self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message) == "Success") {
                                                Core::__Send_Output([
                                                    "status" => true,
                                                    "message" => $message,
                                                    "data" => [
                                                        "otp" => $otp,
                                                        "code" => $otp_expired
                                                    ]
                                                ]);
                                            }else{
                                                Core::__Send_Output([
                                                    "status" => false,
                                                    "message" => "Failed sending message, Development Mode, Trial Account, Cannot send to unverified numbers.",
                                                ]);
                                            }
                                        }
                                    }else{
                                        // Send response otp is sent and cant send more otp in 5 minutes
                                        Core::__Send_Output([
                                            "status" => false,
                                            "message" => "Failed, OTP Verification code already sent to this number",
                                            "date_expired" => $otp_expired
                                        ]);
                                    }
                                }else{
                                    // Send one sms in case database failed creating row
                                    $otp_in_database = RapidDataModel::insert('sms_otp', [
                                        "phone" => Core::__Body_Request()["mobile"],
                                        "code" => $otp,
                                        "date_expired" => $otp_expired
                                    ]);
                                    // If its inserted
                                    if ($otp_in_database) {
                                        // Send SMS
                                        self::__Send_Short_Message(Core::__Body_Request()["mobile"], $message);
                                        Core::__Send_Output([
                                            "status" => true,
                                            "message" => $message,
                                            "data" => [
                                                "otp" => $otp,
                                                "code" => $otp_expired
                                            ]
                                        ]);
                                    }
                                }
                            }
                        }
                    }
        public
            static
                function
                    _verify(){
                        if (Core::__Required_params(["mobile", "code"])) {
                            $verify_otp = RapidDataModel::read('sms_otp', [
                                "select" => "COUNT(*) as Matched",
                                "where" => [
                                    "phone" => Core::__Body_Request()["mobile"],
                                    "code" => Core::__Body_Request()["code"],
                                ]
                            ])["rows"][0]["Matched"];

                            if ($verify_otp > 0) {
                                $delete = RapidDataModel::delete('sms_otp', array(
                                    "key" => ["phone" => Core::__Body_Request()["mobile"]]
                                ));
                                if ($delete) {
                                    Core::__Send_Output([
                                        "status" => true,
                                        "message" => "Sucessfully verified OTP Code"
                                    ]);
                                }else{
                                    Core::__Send_Output([
                                        "status" => false,
                                        "message" => "Failed while trying to verify OTP Code"
                                    ]);
                                }
                            }else{
                                Core::__Send_Output([
                                    "status" => false,
                                    "message" => "Failed, OTP does not matched nor exist",
                                ]);
                            }
                        }
                    }

        public
        	static
        		function
        			_accountkit_send_sms(){
      //   				// error_reporting(0);
						define( "FB_ACCOUNT_KIT_APP_ID", "327540504754544" );
						define( "FB_ACCOUNT_KIT_APP_SECRET", "c5c567ddff515c3ad9f955e5ad3bc951" );
						$ch = curl_init();
						// Set query data here with the URL
						curl_setopt($ch, CURLOPT_URL, "https://www.accountkit.com/v1.0/basic/dialog/sms_login/"); 
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
						curl_setopt($ch, CURLOPT_TIMEOUT, '4');
						$resp = trim(curl_exec($ch));
						print_r($resp);
						curl_close($ch);
						$info = json_decode( $resp, true );
						if( empty( $info ) || !isset( $info['phone'] ) || isset( $info['error'] ) ){
						    return array( "status" => 2, "message" => "Unable to verify the phone number." );
						}
						$phoneNumber = $info['phone']['national_number'];
						echo json_encode( $info );
        			}
	}
  