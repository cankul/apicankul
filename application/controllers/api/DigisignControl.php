<?php  
	class DigisignControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}

        private static function localize_phone_number($phone){
            $indonesia_id = 62;
            $phone_first_digit = substr($phone, 0,1);
            $phone_number = substr($phone, 1);
            if ($phone_first_digit == 0) {
                return $indonesia_id . $phone_number;
            }else{
                return $phone;
            }
        }

        public static function _register(){
              
            if (isset($_FILES["ttd"]["name"])) {
                $ttd_file = new \CurlFile($_FILES["ttd"]["tmp_name"], $_FILES["ttd"]["type"], $_FILES["ttd"]["name"]);
            }

            if (isset($_FILES["fotonpwp"]["name"])) {
               $npwp_file = new \CurlFile($_FILES["fotonpwp"]["tmp_name"], $_FILES["fotonpwp"]["type"], $_FILES["fotonpwp"]["name"]);
            }

            $read_OCR_KTP = RapidDataModel::read("advance_ai_ocr", [
                "where" => [
                    "mobile" => $_POST["mobile"],
                    "endpoint" => "OCR_CHECK_KTP"
                ]
            ])["rows"];

            if (count($read_OCR_KTP) > 0) {

                $read_OCR_KTP = json_decode($read_OCR_KTP[0]["raw_json"]);

                $JSONFILE["JSONFile"] = [];
                $JSONFILE["JSONFile"]["userid"] = "emay@cankul.com";
                $JSONFILE["JSONFile"]["alamat"] = strtolower($read_OCR_KTP->address);
                $JSONFILE["JSONFile"]["jenis_kelamin"] = strtolower($read_OCR_KTP->gender);
                $JSONFILE["JSONFile"]["kecamatan"] = strtolower($read_OCR_KTP->district);
                $JSONFILE["JSONFile"]["kelurahan"] = strtolower($read_OCR_KTP->village);
                $JSONFILE["JSONFile"]["kode-pos"] = '11111';
                $JSONFILE["JSONFile"]["kota"] = strtolower($read_OCR_KTP->city);
                $JSONFILE["JSONFile"]["nama"] = $read_OCR_KTP->name;
                $JSONFILE["JSONFile"]["tlp"] = self::localize_phone_number($_POST["mobile"]);
                // $JSONFILE["JSONFile"]["tlp"] = "6281290100133";
                $JSONFILE["JSONFile"]["tgl_lahir"] = "15-12-1994";
                $JSONFILE["JSONFile"]["provinci"] = strtolower($read_OCR_KTP->province);
                // $JSONFILE["JSONFile"]["idktp"] =  "3201292910970007";
                $JSONFILE["JSONFile"]["idktp"] =  strtolower($read_OCR_KTP->idNumber);
                $JSONFILE["JSONFile"]["tmp_lahir"] = "SERANG";
                $JSONFILE["JSONFile"]["email"] = trim($_POST["email"]);
                $JSONFILE["JSONFile"]["npwp"] = "";

                $JSONFileEncoded = stripcslashes(json_encode($JSONFILE));

                $vars = [
                    "fotoktp" =>  new \CurlFile($_FILES["fotoktp"]["tmp_name"], $_FILES["fotoktp"]["type"], $_FILES["fotoktp"]["name"]),
                    "fotodiri" =>  new \CurlFile($_FILES["fotodiri"]["tmp_name"], $_FILES["fotodiri"]["type"], $_FILES["fotodiri"]["name"]),
                    "jsonfield" => $JSONFileEncoded,
                    "ttd" => $ttd ?? ""
                ]; 
               
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.tandatanganku.com/REG-MITRA.html");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $headers = [
                    'Content-Type: multipart/form', 
                    'Authorization: Bearer qq4dqps7vwspcdf7j3ct3hm8nm7v6y8wfeij6x227yhz71dbvk8obp2lwe8ycq2'
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $server_output = json_decode( curl_exec ($ch));

                curl_close ($ch);

                // print_r($server_output);

                if ($server_output->JSONFile->result == "00") {
                    Core::__Send_Output([
                        "status" => true,
                        "message" =>  $server_output->JSONFile->notif,
                    ]);
                }else{
                    Core::__Send_Output([
                        "status" => false,
                        "message" => $server_output->JSONFile->notif,
                    ]);
                }
            }else{
                Core::__Send_Output([
                    "status" => false,
                    "message" => "OCR_KTP_CHECK Not found",
                ]);
            }

        }

        public static function _send(){

            // Only if mobile phone is included
            if (Core::__Required_Params(["mobile"])) {

                // Read user data
                $user_data = RapidDataModel::read("borrowers", [
                    "where" => [
                        "mobile" => Core::__Body_Request()["mobile"]
                    ]
                ])["rows"];

                if (count($user_data) > 0) {
                    
                    $document_id =  "CDOC" . strtoupper(uniqid());

                    $JSONFILE["JSONFile"] = [];

                    $JSONFILE["JSONFile"]["userid"] = "emay@cankul.com";

                    $JSONFILE["JSONFile"]["document_id"] = $document_id;

                    $JSONFILE["JSONFile"]["send-to"] = [
                       [
                            "name" =>  $user_data[0]["first_name"],
                            // "email" => $user_data[0]["email"],
                            "email" => "robytakwa1@gmail.com",
                       ]
                    ]; 

                    $JSONFILE["JSONFile"]["req-sign"] = [
                       [
                            "name" => $user_data[0]["first_name"],
                            "email" => $user_data[0]["email"],
                            "aksi_ttd" => "mt",
                            "user" => "ttd1",
    /*tambahan*/            "kuser" => "sj2IGxfUgiHEbrzS",
                            "page" => "4",
                            // margin dari kiri
                            "llx" => "40",
                            // Posisi ke atas
                            "lly" => "370",

                            "urx" => "270",

                            "ury" => "580",
                            "visible" => "1"
                       ]
                    ]; 
                   
                    $JSONFileEncoded = stripcslashes(json_encode($JSONFILE));

                    // print_r($_FILES["pdf_file"]);

                    $document_name =  "Cankul User Agreements - ". $user_data[0]["first_name"] . ".pdf";
                    $vars = [
                        "jsonfield" => $JSONFileEncoded,
                        "file" =>  new \CurlFile(FCPATH . "/storage/cankul-contoh.pdf", "application/pdf", $document_name)
                    ]; 
                       
                    $ch = curl_init();
                    // curl_setopt($ch, CURLOPT_URL,"https://api.tandatanganku.com/SNDDOCMITRA.html");
    /*Perubahan*/   curl_setopt($ch, CURLOPT_URL,"https://api.tandatanganku.com/SendDocMitraAT.html");
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    $headers = [
                        'Content-Type: multipart/form', 
                        'Authorization: Bearer qq4dqps7vwspcdf7j3ct3hm8nm7v6y8wfeij6x227yhz71dbvk8obp2lwe8ycq2'
                    ];

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    $server_output = json_decode( curl_exec ($ch));

                    curl_close ($ch);

                    if ($server_output->JSONFile->result == "00") {
                        Core::__Send_Output([
                            "status" => true,
                            "message" =>  $server_output->JSONFile->notif,
                            "data" => [ "document_id" => $document_id ]
                        ]);
                    }else{
                        Core::__Send_Output([
                            "status" => false,
                            "message" => $server_output->JSONFile->notif,
                        ]);
                    }

                }else{
                    // Data User not found
                    Core::__Send_Output([
                        "status" => false,
                        "message" => "User not found",
                    ]);
                }
            }else{
                // Data User not found
                Core::__Send_Output([
                    "status" => false,
                    "message" => "Mobile is required",
                ]);
            }
        }

        public static function _document_status(){

            if (Core::__Required_Params(["document_id"])) {

                $JSONFILE["JSONFile"] = [];

                $JSONFILE["JSONFile"]["userid"] = "emay@cankul.com";

                $JSONFILE["JSONFile"]["pwd"] = "28021997zs";

                $JSONFILE["JSONFile"]["document_id"] = Core::__Body_Request()["document_id"];

                $JSONFileEncoded = stripcslashes(json_encode($JSONFILE));

                $vars = [
                    "jsonfield" => $JSONFileEncoded
                ]; 
                           
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.tandatanganku.com/STATUSDOC.html");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $headers = [
                    'Content-Type: multipart/form', 
                    'Authorization: Bearer qq4dqps7vwspcdf7j3ct3hm8nm7v6y8wfeij6x227yhz71dbvk8obp2lwe8ycq2'
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $server_output = json_decode( curl_exec ($ch));

                curl_close ($ch);


                if ($server_output->JSONFile->result == "00") {
                    // Insert file to database
                    Core::__Send_Output([
                        "status" => true,
                        "data" => [
                            "status" => $server_output->JSONFile->status,
                            // "signed" => $server_output->JSONFile->signed[0]
                        ]
                    ]);
                }else{
                    Core::__Send_Output([
                        "status" => false,
                        "message" => "Failed reading document status, document might be not exist"
                    ]);
                }
            }
        }

        public static function _download(){

            if (Core::__Required_Params(["document_id"])) {

                $JSONFILE["JSONFile"] = [];

                $JSONFILE["JSONFile"]["userid"] = "emay@cankul.com";

                $JSONFILE["JSONFile"]["document_id"] = Core::__Body_Request()["document_id"];

                $JSONFileEncoded = stripcslashes(json_encode($JSONFILE));

                $vars = [
                    "jsonfield" => $JSONFileEncoded
                ]; 
                           
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.tandatanganku.com/DWMITRA64.html");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $headers = [
                    'Content-Type: multipart/form', 
                    'Authorization: Bearer qq4dqps7vwspcdf7j3ct3hm8nm7v6y8wfeij6x227yhz71dbvk8obp2lwe8ycq2'
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $server_output = json_decode( curl_exec ($ch));

                curl_close ($ch);

                if ($server_output->JSONFile->result == "00") {
                    // Insert file to database
                    Core::__Send_Output([
                        "status" => true,
                        "data" => $server_output->JSONFile->file
                    ]);
                }else{
                    Core::__Send_Output([
                        "status" => false,
                        "message" => "Failed downloading file, file might be not exist"
                    ]);
                }
            }

        }
	}


  

