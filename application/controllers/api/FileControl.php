<?php  
	class FileControl extends Core{

		private static $minimum_score_for_photo_with_ktp = 50;

		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		private 
			static
				function
					localize_phone_number($phone){
						$indonesia_id = 62;
						$phone_first_digit = substr($phone, 0,1);
						$phone_number = substr($phone, 1);
						if ($phone_first_digit == 0) {
							return $indonesia_id . $phone_number;
						}else{
							return $phone;
						}
					}

		public 
			static
				function
					_upload(){
						if (Core::__Required_params(["filename", "file"])) {
							$put_image = file_put_contents(
						        FCPATH . "storage/images/". Core::__Body_Request()["filename"],
						        base64_decode(Core::__Body_Request()["file"])
						    );
						    if ($put_image) {
						    	Core::__Send_Output([
						    		"status" => true,
						    		"message" => "Image sucessfully uploaded to /storage/images/"
						    	]);
						    }else{
						    	Core::__Send_Output([
						    		"status" => false,
						    		"message" => "Failed while creating image"
						    	]);
						    }
						}
					}

		public 
			static
				function
					_upload_form(){
						if (!empty($_FILES)) {
							$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
							$image_name = sha1(uniqid()) . "." . $image_ext;
							$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
							if ($upload) {
								Core::__Send_Output([
						    		"status" => true,
						    		"message" => "File uploaded",
						    		"filename" =>  $image_name
						    	]);
							}else{
								Core::__Send_Output([
						    		"status" => false,
						    		"message" => "Failed uploading file, make sure extension is correct etc."
						    	]);
							}
						}else{
							Core::__Send_Output([
						    	"status" => false,
						    	"message" => "There is no file to upload"
						    ]);
						}
					}
		public
			static
				function
					_read_stored_advance_ai($mobile, $field){
						$read = RapidDataModel::read("advance_ai", [
							"where" => [
								"mobile" => $mobile
							]
						])["rows"];
						if (count($read) > 0) {
							Core::__Send_Output([
							  	"status" => true,
								"first_request" => false,
							  	"data" => json_decode($read[0][$field])
							]);
						}else{
							// phone not exist in table
							self::_store_advance_ai();
						}
					}

		public
			static
				function
					_store_advance_ai($image_name){
						$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
						if ($check_ocr->code == "SUCCESS") {
							$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai", [
								"mobile" => $_POST["mobile"],
								"ocr_ktp_check" => json_encode($check_ocr->data),
							]);
							$update_user_data = RapidDataModel::update('borrowers', [
								"key" => [
											"mobile" => $_POST["mobile"]
								],
								"data" => [
									"photo_id_card" => $image_name,
									"first_name" => $check_ocr->data->name,	
									"city" => $check_ocr->data->name,	
									"address" =>  $check_ocr->data->address . " ".  $check_ocr->data->village . " " . $check_ocr->data->rtrw,
									"province" => $check_ocr->data->province
								]
							]);
							if ($update_user_data) {
								Core::__Send_Output([
									"status" => true,
									"data" => $check_ocr->data
								]);
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed while updating user data"
								]);
							}
						}else{
							Core::__Send_Output([
								"status" => false,
								"message" => $check_ocr->message
							]);
						}
					}
		public 
			static
				function
					_upload_form_verify_idcard(){
						// if (!empty($_FILES)) {
						// 	if (isset($_POST["mobile"])) {
								if (!empty($_FILES)) {
									$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
									$image_name = sha1(uniqid()) . "." . $image_ext;
									$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
								}
								if ($upload) {
									$check_mobile = RapidDataModel::read('advance_ai', [
										"where" => [
											"mobile" => $_POST["mobile"],
										]
									])["rows"];	
									if (count($check_mobile) == 0) {
										$check_score = AdvanceAIControl::Verify_IDCard($image_name);
										$insert_score = RapidDataModel::insert('advance_ai', [
											"mobile" => $_POST["mobile"],
											"ocr_photo_with_id" => json_encode($check_score),
										]);
										if ($insert_score == 1) {
											$update_user_data = RapidDataModel::update('borrowers', [
												"key" => [
													"mobile" => $_POST["mobile"]
												],
												"data" => [
													"photo_with_id_card" => $image_name,
												]
											]);
											// tambahan maliki
											$save_image = RapidDataModel::insert("Files", [
												"image_name" => $image_name,
												"mobile" => $_POST["mobile"],												
												"upload_date" =>  date("Y:m:d h:i:s")
											]);
											if ($update_user_data) {
												Core::__Send_Output([
											   		"status" => true,
											   		"first_request" => true,
											   		"message" => "File uploaded",
											   		"score" => $check_score
											   	]);
											}else{
												Core::__Send_Output([
											   		"status" => false,
											   		"message" => "Failed while saving user data",
											   	]);
											}
										}else{
											Core::__Send_Output([
										   		"status" => false,
										   		"message" => "Failed saving score result",
										   	]);
										}
									}else{	
										// if user mobile phone is registered
										// Update the current row insert current
										if ($check_mobile[0]["ocr_photo_with_id"] == NULL) {
											$check_score = AdvanceAIControl::Verify_IDCard($image_name);
											$update_score = RapidDataModel::update('advance_ai', [
												"key" => [
													"mobile" => $_POST["mobile"]
												],
												"data" => [
													"ocr_photo_with_id" => json_encode($check_score)
												]
											]);
											if ($update_score) {
												Core::__Send_Output([
											   		"status" => true,
										   			"first_request" => false,
											   		"data" => $check_score
											   	]);
											}
										}else{
											self::_read_stored_advance_ai($_POST["mobile"], "ocr_photo_with_id");
										}
									}
								}else{
									self::_read_stored_advance_ai($_POST["mobile"], "ocr_photo_with_id");
								}
						// 	}else{
						// 		Core::__Send_Output([
						// 			"status" => false,
						// 			"message" => "Parameter mobile is required"
						// 		]);
						// 	}
						// }else{
						// 	Core::__Send_Output([
						// 		"status" => false,
						// 		"message" => "There is no file to upload"
						// 	]);
						// }
					}

		public
			static
				function
					_upload_form_check_ocr_ktp(){
						// if (!empty($_FILES)) {
							// if (isset($_POST["mobile"])) {
								if (!empty($_FILES)) {
									$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
									$image_name = sha1(uniqid()) . "." . $image_ext;
									$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
								}
								if (isset($upload)) {
									$check_duplicate = RapidDataModel::read("advance_ai", [
										"mobile" => $_POST["mobile"]
									])["rows"];
									if (count($check_duplicate) == 0) {
										self::_store_advance_ai($image_name);
										// tambahan maliki
										$save_image = RapidDataModel::insert("Files", [
											"image_name" => $image_name,
											"mobile" => $_POST["mobile"],
											"upload_date" =>  date("Y:m:d h:i:s")
										]);
									}else{
										// if user mobile phone is registered
										// Update the current row insert current
										if ($check_duplicate[0]["ocr_ktp_check"] == NULL) {
											$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
											$update_field = RapidDataModel::update('advance_ai', [
												"key" => [
													"mobile" => $_POST["mobile"]
												],
												"data" => [
													"ocr_ktp_check" => json_encode($check_ocr)
												]
											]);
											if ($update_field) {
												Core::__Send_Output([
													"status" => true,
													"source" => "database",
		 											"data" => $check_ocr->data
												]);
											}
										}else{
											self::_read_stored_advance_ai($_POST["mobile"], "ocr_ktp_check");
										}
									}
								}else{
									self::_read_stored_advance_ai($_POST["mobile"], "ocr_ktp_check");
								}
						// 	}else{
						// 		Core::__Send_Output([
						// 			"status" => false,
						// 			"message" => "Mobile number required"
						// 		]);
						// 	}
						// }else{
						// 	Core::__Send_Output([
						// 		"status" => false,
						// 		"message" => "There are no file to upload"
						// 	]);
						// }
					}

			private
				static
					function
						_upload_file($temp, $name){
							$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
							$image_name = sha1(uniqid()) . "." . $image_ext;
							$upload = move_uploaded_file($temp, FCPATH . "storage/images/" . $image_name);
						}

			public
				static
					function
						_upload_ocr_check_ktp(){
							error_reporting(0);
							// print_r($_POST);
							if (isset($_POST["mobile"])) {
								$check_data_in_advance_ai = RapidDataModel::read("advance_ai_ocr", [
									"where" => [
										"mobile" => $_POST["mobile"],
										"endpoint" => "OCR_CHECK_KTP"
									]
								])["rows"];
								// Data not Exist in table 
								// Upload new file
								if (count($check_data_in_advance_ai) == 0 ) {
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai_ocr", [
												"endpoint" => "OCR_CHECK_KTP",
												"raw_json" => json_encode($check_ocr->data),
												"mobile" => $_POST["mobile"]
											]);

												// tambahan maliki
												$save_image = RapidDataModel::insert("Files", [
													"image_name" => $image_name,
													"mobile" => $_POST["mobile"],													
													"upload_date" =>  date("Y:m:d h:i:s")
												]);
											$update_user_data = RapidDataModel::update('borrowers', [
												"key" => ["mobile" => $_POST["mobile"]],
												"data" => [
													"photo_id_card" => $image_name,
													"first_name" => $check_ocr->data->name,	
													"city" => $check_ocr->data->name,	
													"address" =>  $check_ocr->data->address . " ".  $check_ocr->data->village . " " . $check_ocr->data->rtrw,
													"province" => $check_ocr->data->province
												]
											]);
											if ($update_user_data) {
												Core::__Send_Output([
													"first_request" => true,
													"status" => true,
													"data" => $check_ocr->data
												]);
											}else{
												Core::__Send_Output([
													"status" => false,
													"message" => "Failed while updating user data"
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
								}else if(count($check_data_in_advance_ai) == 1){
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::OCR_ktp_check($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai_ocr", [
												"endpoint" => "OCR_CHECK_KTP",
												"raw_json" => json_encode($check_ocr->data),
												"mobile" => $_POST["mobile"]
											]);

												// tambahan maliki
												$save_image = RapidDataModel::insert("Files", [
													"image_name" => $image_name,
													"mobile" => $_POST["mobile"],													
													"upload_date" =>  date("Y:m:d h:i:s")
												]);
											$update_user_data = RapidDataModel::update('borrowers', [
												"key" => ["mobile" => $_POST["mobile"]],
												"data" => [
													"photo_id_card" => $image_name,
													"first_name" => $check_ocr->data->name,	
													"city" => $check_ocr->data->name,	
													"address" =>  $check_ocr->data->address . " ".  $check_ocr->data->village . " " . $check_ocr->data->rtrw,
													"province" => $check_ocr->data->province
												]
											]);
											if ($update_user_data) {
												Core::__Send_Output([
													"first_request" => true,
													"status" => true,
													"data" => $check_ocr->data
												]);
											}else{
												Core::__Send_Output([
													"status" => false,
													"message" => "Failed while updating user data"
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
									
								}else{
									// Data Exist
									// Only check existing OCR
									$read = RapidDataModel::read("advance_ai_ocr", [
										"where" => [
											"mobile" => $_POST["mobile"],
											"endpoint" => "OCR_CHECK_KTP"
										]
									])["rows"];
									if (count($read) > 0) {
										Core::__Send_Output([
											"status" => true,
											"first_request" => false,
											"data" => json_decode($read[1]["raw_json"])
										]);
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Data OCR not found"
										]);
									}
								}	
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed, Parameter mobile is required"
								]);
							}
						}

					public static function __ocr_credit_score($mobile, $selfie_score = 0){
						// if (Core::__Required_Params(["mobile"])) {
							$read_user_data = RapidDataModel::read('borrowers', [
								"where" => [
									"mobile" => $mobile
								]
							])["rows"];
							if (count($read_user_data) > 0 ) {
								$check_ocr_in_table = RapidDataModel::read("advance_ai_ocr", [
									"where" => [
										"mobile" => $mobile,
										"endpoint" => "OCR_CREDIT_SCORE"
									]
								])["rows"];
								// Data already exist
								if (count($check_ocr_in_table) > 0) {
									Core::__Send_Output([
										"status" => true,
										"first_request" => false,
										"score" => $selfie_score,
										"credit_score" =>  json_decode($check_ocr_in_table[0]["raw_json"])->score
									]);				
								}else{
									// Data ocr credit score
									// Get Previous Data from Check KTP
									$ocr_check_ktp = RapidDataModel::read("advance_ai_ocr", [
										"where" => [
											"mobile" => $mobile,
											"endpoint" => "OCR_CHECK_KTP"
										]
									])["rows"];
									if (count($ocr_check_ktp) > 0 ) {
										$ocr_check_ktp_id = json_decode($ocr_check_ktp[0]["raw_json"])->idNumber;
										$ocr_check_ktp_name = json_decode($ocr_check_ktp[0]["raw_json"])->name;
										// $ocr_credit_score = AdvanceAIControl::OCR_credit_score(array(
				                        //     'name' => $ocr_check_ktp_name,
										// 	'idNumber' =>  $ocr_check_ktp_id,
				                        //     'phoneNumber' => '+' . self::localize_phone_number($mobile)
										// ));
										$ocr_credit_score = AdvanceAIControl::OCR_advance_score(array(
											'transactionId' => rand(1,1000000),
				                            'name' => $ocr_check_ktp_name,
											'idNumber' =>  $ocr_check_ktp_id,
				                            'phoneNumber' => array(
												'countryCode' => '+62',
												'areaCode'	=>'',
												'number'	=> substr($mobile,1)
											)
										));
										$insert_data_ocr_credit = RapidDataModel::insert("advance_ai_ocr", [
											"mobile" => $mobile,
											"endpoint" => "OCR_CREDIT_SCORE",
											"raw_json" => json_encode($ocr_credit_score->data) 
										]);
										if ($insert_data_ocr_credit) {
											Core::__Send_Output([
												"status" => true,
												"first_request" => true,
												"credit_score" => $ocr_credit_score->data->score,
												"score" => $selfie_score,
												"input" => array(
						                            'name' => $ocr_check_ktp_name,
													'idNumber' =>  $ocr_check_ktp_id,
						                            'phoneNumber' => '+' . self::localize_phone_number($mobile)
												)
											]);	
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => "Failed while trying to insert data",
											]);	
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "You have never had your ID card checked",
										]);	
									}
								}
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Mobile number is not exist",
								]);	
							}
						// }
					}

			public
				static
					function
						_upload_ocr_verify_photo_with_ktp(){
							if (isset($_POST["mobile"])) {
								$check_data_in_advance_ai = RapidDataModel::read("advance_ai_ocr", [
									"where" => [
										"mobile" => $_POST["mobile"],
										"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID"
									]
								])["rows"];
								// Data not Exist in table 
								// Upload new file
								if (count($check_data_in_advance_ai) == 0) {
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::Verify_IDCard($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											if ($check_ocr->data->similarity >= self::$minimum_score_for_photo_with_ktp) {
												$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai_ocr", [
													"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID",
													"raw_json" => json_encode($check_ocr->data),
													"mobile" => $_POST["mobile"]
												]);

												self::__ocr_credit_score($_POST["mobile"], $check_ocr->data->similarity);

												// tambahan maliki
												$save_image = RapidDataModel::insert("Files", [
													"image_name" => $image_name,
													"mobile" => $_POST["mobile"],
													"upload_date" =>  date("Y:m:d h:i:s")
												]);
												// print_r($save_image);

											}else{
												Core::__Send_Output([
													"status" => false,
													"first_request" => true,
													"message" => "Failed identifying your ID"
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
								}else if(count($check_data_in_advance_ai) == 1){
									if (!empty($_FILES)) {
										// Upload new photo KTP
										$image_ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
										$image_name = sha1(uniqid()) . "." . $image_ext;
										$upload = move_uploaded_file($_FILES["image"]["tmp_name"], FCPATH . "storage/images/" . $image_name);
										// User not exist in table
										$check_ocr = AdvanceAIControl::Verify_IDCard($image_name);
										// IF Advance AI Response == successs
										if ($check_ocr->code == "SUCCESS") {
											// Insert a new colum to advance ai table
											if ($check_ocr->data->similarity >= self::$minimum_score_for_photo_with_ktp) {
												$insert_ocr_ktp_check_repsonse = RapidDataModel::insert("advance_ai_ocr", [
													"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID",
													"raw_json" => json_encode($check_ocr->data),
													"mobile" => $_POST["mobile"]
												]);

												self::__ocr_credit_score($_POST["mobile"], $check_ocr->data->similarity);

												// tambahan maliki
												$save_image = RapidDataModel::insert("Files", [
													"image_name" => $image_name,
													"mobile" => $_POST["mobile"],
													"upload_date" =>  date("Y:m:d h:i:s")
												]);
												// print_r($save_image);

											}else{
												Core::__Send_Output([
													"status" => false,
													"first_request" => true,
													"message" => "Failed identifying your ID"
												]);
											}
										}else{
											Core::__Send_Output([
												"status" => false,
												"message" => $check_ocr->message
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "Failed, No file to be upload"
										]);
									}
								}else{
									// Data Exist
									// Only check existing OCR
									$read = RapidDataModel::read("advance_ai_ocr", [
										"where" => [
											"mobile" => $_POST["mobile"],
											"endpoint" => "OCR_VERIFY_PHOTO_WITH_ID"
										],
									])["rows"];
									if (count($read) > 0) {
										$ocr_photo_with_id = json_decode($read[0]["raw_json"]);
										if ($ocr_photo_with_id->similarity >= self::$minimum_score_for_photo_with_ktp) {
											self::__ocr_credit_score($_POST["mobile"], $ocr_photo_with_id->similarity);
											// Core::__Send_Output([
											// 	"status" => true,
											// 	"first_request" => false,
											// 	"score" => $ocr_photo_with_id->similarity
											// ]);

										}else{
											Core::__Send_Output([
												"status" => false,
												"first_request" => false,
												"message" => "Failed identifying your ID"
											]);
										}
									}else{
										Core::__Send_Output([
											"status" => false,
											"message" => "You have never had your Photo With ID card uploaded",
										]);	
									}
								}	
							}else{
								Core::__Send_Output([
									"status" => false,
									"message" => "Failed, Parameter mobile is required"
								]);
							}
						}

	}
