<?php  
    // require_once APPPATH .'libraries/twilio/vendor/autoload.php'; 
    // use Twilio\Rest\Client;
	class InboxControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

        public static function _Count(){
            if (Core::__Required_Params(["borrower_id"])) {
                $unreaded = RapidDataModel::read("inbox_message", [
                    "select" => "COUNT(*) as totalRows",
                    "where" =>  ["is_readed" => false, "borrower_id" =>  Core::__Body_Request()["borrower_id"]]
                ])["rows"];
                $readed = RapidDataModel::read("inbox_message", [
                    "select" => "COUNT(*) as totalRows",
                    "where" =>  ["is_readed" => true, "borrower_id" =>  Core::__Body_Request()["borrower_id"]]
                ])["rows"];
                Core::__Send_Output([
                    "status" => true,
                    "data" => [
                        "unreaded" => intval($unreaded[0]["totalRows"]),
                        "readed" => intval($readed[0]["totalRows"])
                    ], 
                ]);
            }
        }

        public 
            static
                function
                    _Read(){
                        if (empty(Core::__Body_Request()["borrower_id"])) {
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $inbox_message,
                                "message" => "borrower_id kosong"

                            ]);
                        }else{
                            $select = "inbox_message.id as message_id, status, message_header,message,  inbox_message.date_created as date_message, borrower_id, first_name, last_name, mobile, is_readed,";  
                            $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                "join" => array( "borrowers"     => "inbox_message.borrower_id = borrowers.id",),
                                "order" => ["date_created", "DESC"]
                             );
                            $inbox_message = RapidDataModel::read('inbox_message', $condition)["rows"];
                            if (empty($inbox_message)) {
                                Core::__Send_Output([
                                    "status" => true,
                                    "data" => $inbox_message,
                                    "message" => "data kosong"
                                ]);
                            }else{
                                Core::__Send_Output([
                                    "status" => true,
                                    "data" => $inbox_message
                                ]);
                            }
                        }
                    }

        public static function _read_detail(){
            if (Core::__Required_Params(["borrower_id", "message_id"])) {
                $condition = array(
                    "select" => "inbox_message.id as message_id, message, message_header, inbox_message.date_created as date_message, borrower_id, first_name, last_name, mobile, is_readed",
                    "where" => [
                        "inbox_message.borrower_id" => Core::__Body_Request()["borrower_id"],
                        "inbox_message.id" => Core::__Body_Request()["message_id"]
                    ],
                    "join" => array( "borrowers"  => "inbox_message.borrower_id = borrowers.id"),
                 );

                $inbox_message = RapidDataModel::read('inbox_message', $condition)["rows"];

                if (!empty($inbox_message)) {
                    // Read message status
                    if ($inbox_message[0]["is_readed"] == 0 || $inbox_message[0]["is_readed"] == NULL) {
                        RapidDataModel::update("inbox_message", [
                            "key" => ["id" =>  Core::__Body_Request()["message_id"]],
                            "data" => ["is_readed" => true]
                        ]);
                    }
                    Core::__Send_Output([
                        "status" => true,
                        "data" => $inbox_message[0]
                    ]);
                }else{
                    Core::__Send_Output([
                        "status" => false,
                        "message" => "Message not found"
                    ]);
                }
            }
        }

		
       
	}
  

