<?php  
	define("API_FAILED_CONNECT", "Failed while trying to connect to api");
	define("API_FAILED_LOGIN", "Wrong mobile number or password");
	define("API_FAILED_PARAMETER", "Missing Parameter, Check your request");
	define("API_FAILED_INSERT_DATABASE", "Failed processing request, please try again");
	define("API_FAILED_PARAMETER_VALIDATION", "Failed processing request, validation failed");
	define("API_FAILED_404_DATA", "Failed processing request, Data not exist");

	require_once APPPATH . '/libraries/REST_Controller.php';
	class Core extends REST_Controller{
		// Store API Token
		protected static $_API_token = NULL;	
		protected static $_Controller;
		protected static $_errors;
		protected static $_body_request;

		public function __construct(){
			parent::__construct();
			Core::$_Controller = &get_instance();
			$this->load->library(array("Rapid", "Rapid/RapidDataModel", "Rapid/RapidFileManager"));
			self::$_Controller->load->library('user_agent');
		}
		public
			static
				function
					__Check_Token(){
						// Check api token
						// Match them with database
						Core::$_API_token = Core::$_Controller->input->get_request_header('x-api-token') ?? NULL;
						if (Core::$_API_token != NULL) {
							// Match Token within database table accepted_token
							$check_token = RapidDataModel::read('accepted_token', [
								"where" => [
									"token" => Core::$_API_token
								]
							])["rows"];
							// Count rows
							// Check wether its matched
							if (count($check_token) > 0) {
								return $check_token;
							}else{
								Core::$_errors = ["Invalid API Token"];
								return false;
							}
						}else{
							Core::$_errors = ["Missing API Token in your header"];
							return false;
						}
					}
		public
			static
				function
					__Check_User_Token(){
						if (self::__Required_Params(["x-user-token"])) {
							$token = self::__Body_Request()["x-user-token"];
							if (RapidDataModel::is_exist('borrowers_token', ["token" => $token ])) {
								return true;
							}else{
								Core::$_errors = ["Invalid User Token"];
								return false;
							}
						}else{
							Core::$_errors = ["Missing User Token in your body request"];
							return false;
						}
					}
		public
			static
				function
					__Get_Errors(){
						// Return Error
						return implode(", ", Core::$_errors);
					}
		public
			static
				function
					__Body_Request(){
						Core::$_body_request = json_decode(file_get_contents("php://input"));
						return (array) Core::$_body_request;
					}
		public 
			static 
				function 
					__Send_Output($args){
						// Send Output
						if (self::$_Controller->agent->is_browser()){
						    $agent = self::$_Controller->agent->browser().' '.self::$_Controller->agent->version();
						}elseif (self::$_Controller->agent->is_robot()){
						    $agent = self::$_Controller->agent->robot();
						}elseif (self::$_Controller->agent->is_mobile()){
						    $agent = self::$_Controller->agent->mobile();
						}else{
						    $agent = 'Unidentified User Agent';
						}
						self::$_Controller->output->set_content_type('application/json')->set_output(json_encode($args));
					}
		public 
			static 
				function 
					__Required_Params($params){
						$data_req = json_decode(file_get_contents("php://input"));
						$available_params = array();
					    $missing_params = array();
						if (!empty($data_req)) {
					    	foreach ($data_req as $key => $value) {
					    		$available_params[] = $key;
					    	}
					    	// Passed Parameters
					    	foreach ($params as $value) {
					    		if (!in_array($value, $available_params)) {
					    			$missing_params [] = $value;
					    		}
							}
						

					    	if (count($missing_params) > 0) {
					    		self::__Send_Output(array(
									"status" => false,
									"message" => "Parameter  (". implode(", ", $missing_params) . ") is required, please check your request" 
								));
								return false;
					    	}else{
					    		return true;
					    	}
					    }else{
					    	self::__Send_Output(array(
								"status" => false,
								"message" => "Parameter  (". implode(", ", $params) . ") is required, please check your request" 
							));
					    	return false;
					    }
				    }
	}
