<?php  
	class LoanreportControl extends Core{
		
		public function __construct(){
			parent::__construct();
		}

		public
			static
				function
					__Test(){
						echo "Test coming from". __CLASS__;
					}

		public
			static
				function 
					_Myapplication(){

                        $select = "loan_applications.id as id, loan_products.name as name, loan_applications.amount as amount, loan_applications.status as status, loan_applications.notes as notes , loan_applications.created_at as created_at";
                        
                       

                        if (empty(Core::__Body_Request()["borrower_id"])) {
                            $condition = array(
                                "select" => $select,
                                "order" => ["loan_applications.created_at", "DESC"]
                            );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product
                            ]);
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"] ,"status" => Core::__Body_Request()["status"]],
                                "join" => array(
                                        "borrowers"     => "loan_applications.borrower_id = borrowers.id",
                                        "loan_products" => "loan_applications.loan_product_id = loan_products.id"
                                ),
                                "order" => ["loan_applications.created_at", "DESC"]
                             );
                            $loan_product = RapidDataModel::read('loan_applications', $condition)["rows"];
                            Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                        } 

					  

                    }
        public
			static
				function 
					_Loans(){

                        $select = "*";
                        
                       

                        if (empty(Core::__Body_Request()["borrower_id"])) {

                                Core::__Send_Output([
                                "status" => false,
                                "keterangan" => "borrower_id is empty"

                            ]);
                          
                        }else{
                             $condition = array(
                                "select" => $select,
                                "where" => ["borrower_id" => Core::__Body_Request()["borrower_id"]],
                                 "order" => ["id", "DESC"]
                                
                             );
                            $loan_product = RapidDataModel::read('loans', $condition)["rows"];
                             if (empty($loan_product)) {
                            	Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product,
                                "keterangan" => "data kosong"

                            ]);
                            }else{
                            	Core::__Send_Output([
                                "status" => true,
                                "data" => $loan_product

                            ]);
                            }
                        } 
			}
 }