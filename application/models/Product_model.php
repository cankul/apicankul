<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
    protected $db_table_product = "OR_product";
    protected $db_table_product_method = "OR_product_method";

    public function get_product_methods($p_id){
        $this->db->select("*");
        $this->db->from($this->db_table_product_method);
        $this->db->where("product_id", $p_id);
        return $this->db->get()->result_array();
    }
    public function is_product_method_exist($p_id, $m_code){
        $this->db->select("count(*) as exist");
        $this->db->from($this->db_table_product_method);
        $this->db->where("product_id", $p_id);
        $this->db->where("code", $m_code);
        if ($this->db->get()->row()->exist > 0) {
            return 1;
        }else{
            return 0;
        }
    }
}

