<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_model extends CI_Model
{
    protected $db_table_client = "OR_client";
    protected $db_table_client_product = "OR_client_product";

    public function read_once($table, $c_id){
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where("id", $c_id);
        return $this->db->get()->row();
    }

    public function verify_client($csk){
        $this->db->select("count(*) as clientExist");
        $this->db->from($this->db_table_client);  
        $this->db->where("client_secret_key", $csk);
        if ($this->db->get()->row("clientExist") > 0) {
            return true;
        }else{
            return false;
        }
    }
    public function get_product_data($product){
        $this->db->select("*");
        $this->db->from("OR_product");  
        $this->db->where("code", $product);  
        return $this->db->get()->row() ?? false;
    }
    public function get_client_access_key($csk, $product){
        $this->db->select("OR_client_product.client_access_key");
        $this->db->from("OR_client");  
        $this->db->join("OR_client_product", "OR_client_product.client_id = OR_client.id");  
        $this->db->where("OR_client.client_secret_key", $csk);
        $this->db->where("OR_client_product.product_id", $product);
        $this->db->where("OR_client_product.is_activated", 1);
        return $this->db->get()->row()->client_access_key ?? false;
    }
    public function get_client_product_data($cak, $product_id){
        $this->db->select("*");
        $this->db->from("OR_client_product");
        $this->db->where("client_access_key", $cak);
        $this->db->where("product_id", $product_id);
        return $this->db->get()->row();
    }
}

