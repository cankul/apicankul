<?php 
if ( ! function_exists('timeAgo'))
	{
	
	function timeAgo($datetime, $full = false) {
	    $now = new DateTime;
		$ago = new DateTime('@'.$datetime);
		return date("d M Y", $datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}

if ( ! function_exists('dateDiffFromToday'))
        {

        function dateDiffFromToday($date) {
            $now = time();
			$your_date = strtotime($date);
			$datediff = abs($your_date - $now);
			return intval($datediff / 86400)+1;
        }
}

if ( ! function_exists('dateFormat'))
	{
	
	function dateFormat($datetime, $full = false) {
	    $now = new DateTime;
		$ago = new DateTime('@'.$datetime);
		return date("d M Y at H:i", $datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}

if ( ! function_exists('dateForm'))
	{
	
	function dateForm($datetime, $full = false) {
	    $now = new DateTime;
		$ago = new DateTime('@'.$datetime);
		return date("l, d M Y", $datetime);
	}
}

if ( ! function_exists('timeForm'))
	{
	
	function timeForm($datetime, $full = false) {
	    $now = new DateTime;
		$ago = new DateTime('@'.strtotime($datetime));
		return date("H:i", strtotime($datetime));
	}
}