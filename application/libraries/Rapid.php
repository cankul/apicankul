<?php  
	class Rapid extends CI_Model{
		
		protected static $_start;
		protected static $_selected_code;
		protected static $_client_access_key;
		protected static $_selected_table;
		protected static $_selected_table_fields = array();
		protected static $_selected_product_id;
		protected static $ci;

		public function __construct($args = array()){
			parent::__construct();
			self::$ci = &get_instance();
			self::$ci->load->library('user_agent');
			if (!empty($args)) {
				// Use default database configuration
				RapidDataModel::use('default');
				// Store selected table to $_selected_table variable
				self::$_selected_table = RapidDataModel::read('OR_product_method', array(
					"select" => "table",
					"where" => array(
						"code" => $args["method"]
					)
				))["rows"][0]["table"];
				// Store selected product id base on code argument passed
				self::$_selected_product_id = RapidDataModel::read('OR_product', array(
					"select" => "id",
					"where" => array(
						"code" => $args["code"]
					)
				))["rows"][0]["id"];
				self::$_selected_code = $args["code"];
			}
		}
		public static function __body_request(){
			return json_decode(file_get_contents("php://input"), TRUE);
		}
		public static function __send_output($args){
			if (self::$ci->agent->is_browser()){
			    $agent = self::$ci->agent->browser().' '.self::$ci->agent->version();
			}elseif (self::$ci->agent->is_robot()){
			    $agent = self::$ci->agent->robot();
			}elseif (self::$ci->agent->is_mobile()){
			    $agent = self::$ci->agent->mobile();
			}else{
			    $agent = 'Unidentified User Agent';
			}
			$docs = self::$ci->uri->segment_array();
			unset($docs[1]);
			$output = array(
				"execution_info" => array(
					"date" => date("Y/m/d H:i:s", intval(self::__result_count_exec_time())),
					"timestamp" => self::__result_count_exec_time(),
					"platform" => $agent ?? "",
					"user_agent" => self::$ci->agent->agent_string(),
					"ip_address" => self::$ci->input->ip_address(),
					"uri_string" => urldecode(self::$ci->uri->uri_string()),
					"docs" => "http://www.openrapid.com/docs/" . implode("/", $docs)
				)
			);
			if (!empty($args["data"]) && is_array($args["data"])) {
				$output["execution_info"]["total_result"] = count($args["data"]);
			}
			$output["result"] = $args;
			self::$ci->output->set_content_type('application/json')->set_output(json_encode($output));
		}
		public static function __start_count_exec_time(){
	        $time = microtime();
	        $time = explode(' ', $time);
	        $time = $time[1] + $time[0];
	        $start = $time;
	       	self::$_start = $start;
	    }
	    public static function __result_count_exec_time(){
	        $time = microtime();
	        $time = explode(' ', $time);
	        $time = $time[1] + $time[0];
	        $finish = $time;
	        $total_time = round(($finish - self::$_start), 4);
	        return $total_time;
	    }
	    public static function __display_error($code, $action = ""){
	    	$errors = array(
	    		"302" => array(
	    			"status" => 0,
	    			"code" => $code,
	    			"message" => "Oh no! We couldn't find the data you were looking for"
	    		),
	    		"202" => array(
	    			"status" => 0,
	    			"code" => $code,
	    			"message" => $action ?? "Failed creating query, Please try again"
	    		)
	    	);
	    }
	    public static function __required_params($params){
			$data_req = json_decode(file_get_contents("php://input"));
			$available_params = array();
		    $missing_params = array();
			if (!empty($data_req)) {
		    	foreach ($data_req as $key => $value) {
		    		$available_params[] = $key;
		    	}
		    	// Passed Parameters
		    	foreach ($params as $value) {
		    		if (!in_array($value, $available_params)) {
		    			$missing_params [] = $value;
		    		}
		    	}
		    	if (count($missing_params) > 0) {
		    		self::__send_output(array(
						"status" => 0,
						"error_code" => 201,
						"error_message" => "Parameter (". implode(", ", $missing_params) . ") is required, please check your request" 
					));
					return false;
		    	}else{
		    		return true;
		    	}
		    }else{
		    	self::__send_output(array(
					"status" => 0,
					"error_code" => 201,
					"error_message" => "Parameter (". implode(", ", $params) . ") is required, please check your request" 
				));
		    	return false;
		    }
	    }
	    public function __put_tracker($status){
	    	return 
	    		self::$ci->System_model->put_tracker(array(
	    		"product_code" => self::$requested_uri["product"], 
	    		"product_method" => self::$requested_uri["function"].self::$requested_uri["method"],
	    		"client_access_key" => self::$requested_uri["client_access_key"],
	    		"client_secret_key" => self::$requested_uri["client_secret_key"],
	    		"ip_address" => self::$ci->input->ip_address(),
	    		"date_created" =>  date("Y-m-d H:i:s"),
	    		"geolocation" => "",
	    		"status" => $status
	    	));
	    }
	    public function __validated_client_access_key(){
	    	RapidDataModel::use('default');
	    	if (!empty($client_access_key = self::$ci->input->get_request_header('client_access_key'))) {
	    		$is_validated = RapidDataModel::read('OR_client_product', array(
	    			"select" => "COUNT(*) as Validated",
	    			"where" => array(
	    				"product_id" => self::$_selected_product_id,
	    				"client_access_key" => $client_access_key
	    			)
	    		))["rows"][0]["Validated"];
	    		if ($is_validated > 0) {
	    			self::$_client_access_key = $client_access_key;
	    			RapidDataModel::use(self::$_selected_code);
	    			return true;
	    		}else{
	    			// Invalid Client Access
					self::__send_output(array(
						"status" => false,
						"error_found" => false,
		    			"error_code" => 190,
		    			"error_message" => "Invalid client_access_key, Make sure you have registered your product"
		    		));
	    			return false;
	    		}
	    	}else{
	    		// Invalid Client Access
				self::__send_output(array(
					"status" => false,
					"error_found" => false,
		    		"error_code" => 190,
		    		"error_message" => "Invalid client_access_key, Please check your header"
		    	));
	    		return false;
	    	}
	    }
	}
?>